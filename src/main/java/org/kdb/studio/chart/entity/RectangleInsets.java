package org.kdb.studio.chart.entity;

public class RectangleInsets implements Overridable<RectangleInsets> {
    public double top;
    public double bottom;
    public double left;
    public double right;

    public double getTop() {
        return top;
    }

    public void setTop(double top) {
        this.top = top;
    }

    public double getBottom() {
        return bottom;
    }

    public void setBottom(double bottom) {
        this.bottom = bottom;
    }

    public double getLeft() {
        return left;
    }

    public void setLeft(double left) {
        this.left = left;
    }

    public double getRight() {
        return right;
    }

    public void setRight(double right) {
        this.right = right;
    }

    @Override
    public void override(RectangleInsets obj) {
        Overridable.overrideObject(this, obj);
    }
}
