package org.kdb.studio.chart.entity;

public enum FontAttributes {
    PLAIN, BOLD, ITALIC
}
